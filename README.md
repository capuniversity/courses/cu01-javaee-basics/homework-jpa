# jpa Assignment

Your task is to resolve all the `TODO` statements in the code and make the tests pass. 
Run the project build by invoking:

```
mvn clean package
```
How to run from Idea? see screenshot ![gif](docs/howto-arquillian-idea.gif)

Few helpful tips:

Do not forget to stop any other JBoss instance before you run the tests.

* IntelliJ > View > tool windows > TODO
* Eclipse > Window > View > Tasks - will show you all the TODOs nicely
* Check the server output for errors during startup, every ERROR message is important
* There are more ways to solve the tasks, but it is possible to solve all of them without the need for any   
  additional classes or methods.
* you can check what is the DB schema exported by your model by running mvn clean package -Dmaven.test.skip=true -Pschema-gen` this will spill out an sql into target/schema/schema.sql

Good luck