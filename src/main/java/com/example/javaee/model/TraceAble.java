package com.example.javaee.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

//TODO we want attributes from this class to be present on every entity that extends it
//HINT: look for MappedSupperclass in Hibernate docs
//TODO you can use the Entity lifecycle event methods here(at the end of the file), or create your one EntityListener
//HINT: look for lifecycle listeners in hibernate docu
@Getter
@Setter
public class TraceAble {

    //TODO we want this date to be filled with current date when our entity is persisted
    private Date creationDate;

    //TODO we want this date to be filled with current date when our netity is updated
    private Date updateDate;

    //TODO: automatically set this to true when entity has been loaded from DB
    private boolean loadedFromDB;


    public void postLoad() {
       //TODO make me useful (hint: lifecycle events postLoad)
    }

    public void preUpdate() {
      //TODO make me useful (hint: lifecycle events preUpdate, good place to record UpdateDate)
    }

    public void prePersist() {
      //TODO make me useful(hint: lifecycle events prePersist, good place to record creationDate)
    }

}
