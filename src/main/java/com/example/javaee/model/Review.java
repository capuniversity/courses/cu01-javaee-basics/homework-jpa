package com.example.javaee.model;

import lombok.Getter;
import lombok.Setter;

/**
 * The Book review entity
 *
 * @author Matus
 *
 * TODO: make this a DB entity with table name B_REVIEW with auto generated LONG id.
 * TODO: we want to track creation/update date of this entity(see TODOs in TraceAble class)
 */
@Getter
@Setter
public class Review extends TraceAble {
    private Long id;

    //TODO: we want to store longer text here - up to 1000 characters
    // Hint: see @Column, or google how to control col length
    private String text;

    //TODO mandatory, lazy loaded book reference, add required mapping
    // Hint: Bidirectioal oneToMany, optional=false
    private Book book;

    // String identifier of the user who created the review.
    // TODO: DB wont be very happy with the naming of this attribute('from' is a reserved keyword), but your manager insists that it has to be named like that in your model..
    private String from;


}
