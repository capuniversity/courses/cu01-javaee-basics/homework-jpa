package com.example.javaee.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

import javax.persistence.Entity;

/**
 * The book publishing company.
 *
 * @author Matus
 *
 * TODO: make me an entity stored in DB table B_PUBLISHER, with auto generated LONG id.
 * TODO: we want to track the creation/update date of this entity(see TODOs in TraceAble).
 */
@Getter
@Setter
public class Publisher extends TraceAble {

    //TODO: make me an autogenarated PK
    private Long id;

    private String name;

    //TODO: lazy loaded, with FK in Book table named PUBLISHER_ID
    private Set<Book> publishedBooks;

    //TODO: we want all the attributes from Address stored within publisher table(hint: google hibernate Embedable)
    private Address address;

}
