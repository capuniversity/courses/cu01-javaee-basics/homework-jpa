package com.example.javaee.model;

import lombok.Getter;
import lombok.Setter;

/**
 * A simple address object
 *
 * @author Matus Majchrak
 *
 * TODO: addresses should not be stored in separate table,
 * but rather included in the table of the owning entity
 */
@Getter
@Setter
public class Address {

    private String city;

    private String street;

    private String postalCode;

    private String country;

    public Address() {

    }

    public Address(String city, String street, String postalCode, String country) {
        super();
        this.city = city;
        this.street = street;
        this.postalCode = postalCode;
        this.country = country;
    }



}
