package com.example.javaee.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.Set;

/**
 * The Book entity.
 *
 * @author Matus
 *
 * TODO: make this a DB entity with table name B_BOOK, IBSN  acts as table ID.
 * TODO: we want to track creation/update date of this entity
 */
@Getter
@Setter
public class Book extends TraceAble {

    // ISBN is a unique identifier of every book, make it the primary key
    private String isbn;

    private String name;

    // TODO many books have 1 author, add mapping, make it lazy loaded
    private Author author;

    // TODO: lazy loaded reviews,add required mapping annotations
    private Set<Review> reviews;

    //TODO add unidirectional onetomany mapping
    private Publisher publisher;

    private Date publishDate;

    // TODO: add required mapping, we want a very simple java model with a String set of tags(scifi,novel,drama...)
    private Set<String> tags;



}
